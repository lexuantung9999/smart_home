#include <Servo.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>


#define BLYNK_PRINT Serial

char auth[] = "1pM9Jv2sdawJIaW90BswAiVMGzKRDn6E";

char ssid[] = "smarthome";
char pass[] = "smarthome123";


BlynkTimer timer;

WidgetLCD lcd1(V0);
WidgetLCD lcd2(V1);

#define Servo1_pin D5
#define Servo2_pin D6 

#define LED1  D1
#define LED2  D2
#define LED3  D0

#define FAN   D7





Servo Servo1;
Servo Servo2;
String data_uart;
char text_temp[20];
char text_gas[20];
int servo_pin_check;

int max_gas = 50; //%
int max_temp = 40;

int count_warning;
long times1=0;
int stt_door ; 
int id;
int stt_warning;
int last_stt_door=0;
int led_stt[3],fan_stt,door_stt;
int button_stt[6];
void setup() {
  // put your setup code here, to run once:
  pinMode(LED1,OUTPUT);
  pinMode(LED2,OUTPUT);
  pinMode(LED3,OUTPUT);
  pinMode(FAN,OUTPUT);
  pinMode(A0,INPUT);
  digitalWrite(LED1,0);
  digitalWrite(LED2,0);
  digitalWrite(LED3,0);
  digitalWrite(FAN,0);

  Servo1.attach(Servo1_pin);
  Servo1.write(10); // dong cua
  Serial.begin(115200);

  Blynk.begin(auth, ssid, pass);
  lcd1.clear(); 
  lcd1.print(0, 0, "Nhiet do(oC):  "); 
  lcd2.clear();
  lcd2.print(0, 0, "Khi Gas(0-100%): ");
  timer.setInterval(120L, check_button);
}


BLYNK_WRITE(V4)
{
  led_stt[0] = param.asInt();
  digitalWrite(LED1,led_stt[0]);
}

BLYNK_WRITE(V5)
{
 led_stt[1] = param.asInt();
  digitalWrite(LED2,led_stt[1]);
}
BLYNK_WRITE(V6)
{
  led_stt[2] = param.asInt();
  digitalWrite(LED3,led_stt[2]);
}
BLYNK_WRITE(V7)
{
  fan_stt = param.asInt();
  digitalWrite(FAN,fan_stt);
}


BLYNK_WRITE(V2)
{
  servo_pin_check = param.asInt();
  if(servo_pin_check == 0){
      Servo1.write(10); // dong cua
      last_stt_door=0;
      times1 = millis();
    }
  if(servo_pin_check ==1){
       Servo1.write(80); // mo cua
       last_stt_door=1;
       times1 = millis();
    }
}

BLYNK_WRITE(V3){
   int warning_pin = param.asInt();
   if (warning_pin == 0){
      digitalWrite(LED1,0);
      digitalWrite(LED2,0);
      digitalWrite(LED3,0);
      digitalWrite(FAN,0);
   
      Servo1.write(10); // dong cua
    }  
}

void check_button(){
 
  if(analogRead(A0)>=720 && analogRead(A0)<790){
    while(analogRead(A0)<1020);
    led_stt[0] = digitalRead(LED1);
    digitalWrite(LED1,!led_stt[0]);
    Blynk.virtualWrite(V4,!led_stt[0]);
  }

  if(analogRead(A0)>=600&&analogRead(A0)<705){
    while(analogRead(A0)<1020);
    led_stt[1] = digitalRead(LED2);
    digitalWrite(LED2,!led_stt[1]);
    Blynk.virtualWrite(V5,!led_stt[1]);
  }

   if(analogRead(A0)>=800&&analogRead(A0)<899){
    while(analogRead(A0)<1010);
    led_stt[2] = digitalRead(LED3);
    digitalWrite(LED3,!led_stt[2]);
    Blynk.virtualWrite(V6,!led_stt[2]);
  }


  if(analogRead(A0)>=950&&analogRead(A0)<1015){
    while(analogRead(A0)<1020);
    fan_stt = digitalRead(FAN);
    digitalWrite(FAN,!fan_stt);
    Blynk.virtualWrite(V7,!fan_stt);
  }

  if(analogRead(A0)>=900&&analogRead(A0)<950){
    while(analogRead(A0)<1020);
    if(last_stt_door==1){
       Servo1.write(10); // dong cua  
    }
    if(last_stt_door==0){
      Servo1.write(80); // mo cua  
     }
    last_stt_door = !last_stt_door;
    Blynk.virtualWrite(V2,last_stt_door);
  }
  

  
  
  
}
void json_test(void){
        String data_uart ="";
        if (Serial.available()>0){
              data_uart = Serial.readString();
          }
       
        if (data_uart.length() > 0){
              StaticJsonDocument<200> doc;
              DeserializationError error = deserializeJson(doc, data_uart);
              if (error){
                    Serial.print("error");
                    Serial.println(error.c_str());
                    Serial.println();
                    return;
                }
               
              int temp1 = doc["data"][0];
              int temp2 = doc["data"][1];
              int temp3 = doc["data"][2];
              int gas1 =  doc["data"][3];
              int gas2 =  doc["data"][4];
              int gas3 =  doc["data"][5];
              int stt_vantay = doc["data"][6];
              int id = doc["data"][7];

              sprintf(text_temp,"%d  %d  %d     ",temp1,temp2,temp3);
              sprintf(text_gas,"%d  %d  %d     ",gas1,gas2,gas3);
              lcd1.print(0, 0, "Nhiet do(oC):       "); 
              lcd2.print(0, 0, "Khi Gas(0-100%):      ");
              lcd1.print(0,1,text_temp);
              lcd2.print(0,1,text_gas);

              
              if (temp1 >= max_temp || gas1 >= max_gas) {
                    stt_warning = 1;
                   
                    Blynk.virtualWrite(V3,1);
                    lcd1.print(0,0,"phong 1 co chay!");
                    lcd2.print(0,0,"phong 1 co chay!");
                    lcd1.print(0,1,text_temp);
                    lcd2.print(0,1,text_gas);
                    digitalWrite(FAN,1);
                    Servo1.write(80); // mo cua
                    times1 = millis();
                    
              }

             if (temp2 >= max_temp || gas2 >= max_gas) {
                    stt_warning =1;
                   
                    Blynk.virtualWrite(V3,1);
                    lcd1.print(0,0,"phong 2 co chay!");
                    lcd2.print(0,0,"phong 2 co chay!");
                    lcd1.print(0,1,text_temp);
                    lcd2.print(0,1,text_gas);
                    digitalWrite(FAN,1);
                    Servo1.write(80); // mo cua
                    times1 = millis();
              }
             if (temp3 >= max_temp || gas3 >= max_gas) {
                    stt_warning =1;
                    Blynk.virtualWrite(V3,1);
                    lcd1.print(0,0,"phong 3 co chay!");
                    lcd2.print(0,0,"phong 3 co chay!");
                    lcd1.print(0,1,text_temp);
                    lcd2.print(0,1,text_gas);
                    digitalWrite(FAN,1);
                    Servo1.write(80); // mo cua
                    
                    times1= millis();
              }

            if(stt_warning==1){
              
             if ( (millis() - times1) > 5000){
                     Servo1.write(10);   //dong cua
                     digitalWrite(FAN,0);  
                      stt_warning=0;
             }
              
            }

            if (temp3 < max_temp && gas3 < max_gas &&temp2 < max_temp && gas2 < max_gas&&temp1 < max_temp && gas1 < max_gas){
                  stt_warning = 0;
                }
             if (stt_vantay == 2){                    
                lcd1.print(0, 0, "CANH BAO TROM!!  "); 
                lcd1.print(0,1,"SAI MA 3 LAN!!   ");
                lcd2.print(0, 0, "CANH BAO TROM!!   ");
                lcd2.print(0,1,"SAI MA 3 LAN!!   ");
                    Servo1.write(10); // dong cua   
                    times1 = millis();                
                }
              if (id!=0){
                   
                        Servo1.write(80); // mo cua
                        lcd1.clear();
                        lcd1.print(0,1,"DANG NHAP ID#");
                        lcd1.print(12,1,id);
                        lcd2.clear();
                        lcd2.print(0,1,"Mo cua  ");
                        delay(2000);
                        times1 = millis();
                         last_stt_door=1;
                }
                if(last_stt_door==1){
                      if(millis()-times1>5000){
                            Servo1.write(10); // dong cua 
                            last_stt_door=0;
                        }
                        
                 }
               
                    
          }    

  }
void loop() {
  json_test(); 
  Blynk.run();
  timer.run();

 
  
  //Serial.write(Serial.read());
}
