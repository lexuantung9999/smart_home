#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <ArduinoJson.h>
#include <Adafruit_Fingerprint.h>
LiquidCrystal_I2C lcd(0x27,16,2);

SoftwareSerial mySerial(6, 7);  //giao tiep voi vantay

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

SoftwareSerial phoneSerial(10, 11); //giao tiep voi sim

String my_Phone  = "+84329074333";


#define GAS1  A3
#define GAS2  A6
#define GAS3  A7

#define TEMP3  A2
#define TEMP2  A1
#define TEMP1  A0


#define BUZZ 8

#define SCL A5
#define SDA A4

char text1[20];
char text2[20];

int temp_max =40;
int gas_max = 50;


long times1;
int modes = 0;
int cmode = 0;
int id = 1;
int number = 0;
int count = 1;
int stt_vantay=0;
int data_count=0;
int count_loi;
int id_send;
int count_stt_vantay;
int stt_warning=0;
int temp1;int temp2;
int temp3;
int gas1;
int gas2;
int gas3;
uint16_t cnt=0, sum[6]={0,0,0,0,0,0};
long time_mode;


int temp_read(uint8_t number){
      uint8_t sensor;
      int analog_val;
      switch(number){
            case 1:
              sensor = TEMP1;
              break;
            case 2:
              sensor = TEMP2;
              break;
            case 3:
              sensor = TEMP3;
              break;
        }
      analog_val = analogRead(sensor);
      float vol = analog_val*5.5/1024.0;
      float temp = vol*100-5;
      return(temp);
}
void read_data(void){
    
    temp1 = temp_read(1);
    temp2 = temp_read(2);
    temp3 = temp_read(3);
    gas1 = analogRead(GAS1);
    gas2 = analogRead(GAS2);
    gas3 = analogRead(GAS3);

}
void send_data(void)
{
    cnt++;    
    if (cnt>10)
    {
        // tinh tong gia tri nhiet do 10 lan roi chia trung binh
        for (int j=0;j<6;j++) sum[j]/=10;

        sum[3] = sum[3]*0.0978;
        sum[4] = sum[4]*0.0978;
        sum[5] = sum[5]*0.0978;
        
        if (stt_vantay ==2){
            count_stt_vantay +=1;
        }
        if (count_stt_vantay == 3){
            count_stt_vantay = 0;
            stt_vantay = 0;
        }
        
        StaticJsonDocument<200> doc;
        
        JsonArray data = doc.createNestedArray("data");


        if (sum[0] >= temp_max || sum[1] >= temp_max || sum[2]>=temp_max || sum[3] >= gas_max||sum[4]>=gas_max || sum[5]>=gas_max)
        {
            stt_warning =1;
            digitalWrite(BUZZ,1);
        }
        else {
             stt_warning = 0; 
             digitalWrite(BUZZ,0);
         }

        // thiet lap chuoi de gui
        data.add(sum[0]);
        data.add(sum[1]);
        data.add(sum[2]);
        data.add(sum[3]);
        data.add(sum[4]-20);
        data.add(sum[5]);
        data.add(stt_vantay);
        data.add(id_send);
        data.add(stt_warning);
        serializeJson(doc, Serial);//gui chuoi

        // hien thi len man hinh
        sprintf(text1,"T: %d  %d  %d oC   ",sum[0],sum[1],sum[2]);
        lcd.setCursor(0,0);
        lcd.print(text1);
        Serial.println(text1);
        // nhiet do
        
        sprintf(text2,"G: %d  %d  %d  ",sum[3],sum[4]-20,sum[5]);
        lcd.setCursor(0,1);
        lcd.print(text2);
        lcd.setCursor(14,1);
        lcd.print("%  ");
        Serial.println(text2);
        // khi gas
        

        id_send = 0 ;
        stt_vantay = 0;
        for (int j=0;j<6;j++){
            sum[j]=0;
            cnt=0;
        }
        for(int i=0;i<6;i++){
                sum[i] = 0;
             }
        }
         else
        {
      
        read_data();
        sum[0] += temp1;
        sum[1] += temp2;
        sum[2] += temp3;
        sum[3] += gas1;
        sum[4] += gas2;
        sum[5] += gas3;       
        }  
}


void make_call(void){

  

        mySerial.end();
        delay(20);
       



       if (stt_warning == 1){
                Serial.println("warning");
        lcd.clear();
        lcd.print("Canh bao chay !! " );
         phoneSerial.println("ATE0"); 
        delay(50);
        phoneSerial.begin(9600);
                //goi dien
        //phoneSerial.println("ATD" + my_Phone +";");
        phoneSerial.println("ATD"+my_Phone+";");
        delay(10000);
        //phoneSerial.println("ATH");
        delay(50);
                //AT+CMGF=1 Lệnh đưa SMS về chế độ Text , phải có lệnh này mới gửi nhận tin nhắn dạng Text
        phoneSerial.println("AT+CMGF=1");
        delay(50);
         //gui tin nhan
          phoneSerial.println("AT+CMGF=1");  delay(50);
        // lệnh gửi 1 tin nhắn đến số điện thoại cho trước
        phoneSerial.println("AT+CMGS=\"" + my_Phone+ "\"");  delay(50);
        // nội dung tin nhắn
        phoneSerial.println("Canh bao chay!"); delay(20);
        // gửi mã 0x1a để kết thúc nội dung tin nhắn
        phoneSerial.write(0x1a);  delay(50);

        delay(500);
         }

       if (stt_vantay == 2){
                  //goi dien
        lcd.clear();
        lcd.print("Canh bao trom!! " );
        //phoneSerial.println("ATD" + my_Phone +";");
         phoneSerial.println("ATE0"); 
        delay(50);
        phoneSerial.begin(9600);
        phoneSerial.println("ATD"+my_Phone+";");
        delay(10000);
        //phoneSerial.println("ATH");
                  //AT+CMGF=1 Lệnh đưa SMS về chế độ Text , phải có lệnh này mới gửi nhận tin nhắn dạng Text
        phoneSerial.println("AT+CMGF=1");
        delay(50);
         //gui tin nhan
          phoneSerial.println("AT+CMGF=1");  delay(50);
        // lệnh gửi 1 tin nhắn đến số điện thoại cho trước
        phoneSerial.println("AT+CMGS=\"" + my_Phone+ "\"");  delay(50);
        // nội dung tin nhắn
        phoneSerial.println("Canh bao trom!");
        delay(20);
        // gửi mã 0x1a để kết thúc nội dung tin nhắn
        phoneSerial.write(0x1a);  
        delay(50);

        delay(500);
         }
        stt_warning = 0 ;
        stt_vantay=0;
        finger.begin(57600);
        phoneSerial.end();
  }

  
void tich()
{
  digitalWrite(13,0);
  delay(200);
  digitalWrite(13,1);
}
int laymau(int _id)
{
  // lay mau van tay
  int p = -1;
  lcd.setCursor(0,0); lcd.print("  Them van tay: ");
  lcd.setCursor(0,1); lcd.print("ID"); lcd.print(_id);
  lcd.print("              ");
  while (p != FINGERPRINT_OK) {
  
  p = finger.getImage();
    if (p==FINGERPRINT_OK) tich();
  }
  p = finger.image2Tz(1);
  if (p==FINGERPRINT_OK) 
  { 
    lcd.setCursor(7,1); lcd.print("Luu lan 1"); 
  }
  delay(1000);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  p = -1;
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    if (p==FINGERPRINT_OK) tich();
  }
  p = finger.image2Tz(2);
  if (p==FINGERPRINT_OK)
  {
    lcd.setCursor(7,1); lcd.print("Luu lan 2"); 
  }
  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
//    Serial.println("Prints matched!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
//    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
//    Serial.println("Fingerprints did not match");
    return p;
  } else {
//    Serial.println("Unknown error");
    return p;
  }      
  p = finger.storeModel(_id);
  if (p == FINGERPRINT_OK) {
    lcd.setCursor(7,1); lcd.print(" Da Luu! ");
//    Serial.println("Stored!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
//    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
//    Serial.println("Could not store in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
//    Serial.println("Error writing to flash");
    return p;
  } else {
//    Serial.println("Unknown error");
    return p;
  }
  return -2;
}
int getFinger() {
 // quet van tay
  uint8_t p = finger.getImage();

  if (p != FINGERPRINT_OK){  
    return -1;
  }

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK){
    return -2;
  }

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK){
    return -3;
  }
 
//  Serial.print(" with confidence of "); Serial.println(finger.confidence);
  return finger.fingerID; 
}
void check_id(void){
                id = getFinger();
          if(id>0)
          {   
                  count_loi = 0;
                  lcd.clear();
                  lcd.setCursor(0,0);
                  lcd.print("Co van tay");
                  lcd.setCursor(0,1);
                  lcd.print("Dang nhap ID#"); lcd.print(finger.fingerID);
                  id_send = finger.fingerID;
                  stt_vantay = 1;   
                  delay(1000);  
                  lcd.clear();
          }
                
          if (getFinger() == -3)
          {
                      stt_vantay = 0;
                      lcd.clear();
                      lcd.setCursor(0,0);
                      lcd.print("Co van tay");
                      lcd.setCursor(0,1);
                      lcd.print("Loi van tay !!!"); 
                      count_loi+=1;
                      lcd.setCursor(12,1);
                      lcd.print(count_loi);
                      if(count_loi>=3)
                      {
                            stt_vantay = 2;
                           
                            lcd.setCursor(0,0);
                            lcd.print("Sai qua 3 lan   ");
                            digitalWrite(BUZZ,1);
                            lcd.setCursor(0,1);
                            delay(1000);
                            lcd.clear();
                            lcd.print("Cho chut.......");
                            count_loi=0 ;             
                      }
                      delay(1000);  
                      lcd.clear();
                      digitalWrite(BUZZ,0);
             }

         
  }
void change_mode(){
      if( cmode == 0 ){
          modes = 1;
      }
       else if (cmode !=0 ) {
      
        }
      time_mode = millis();
  }

  
void setup() {
  
  pinMode(GAS1,INPUT);  pinMode(GAS2,INPUT);  pinMode(GAS3,INPUT);
  pinMode(TEMP1,INPUT); pinMode(TEMP2,INPUT); pinMode(TEMP3,INPUT);
  pinMode(2,INPUT_PULLUP);
  pinMode(3,INPUT_PULLUP);
  pinMode(BUZZ,OUTPUT);
 
  lcd.init();
  lcd.backlight();
  lcd.print("Smart home");
  delay(1000);
  lcd.setCursor(0,0); lcd.print("Tim thiet bi.....");
    
  delay(500);
  Serial.begin(115200);
  finger.begin(57600);
  if (finger.verifyPassword()) {
   // tich();
    lcd.setCursor(0,1); lcd.print("      Xong!     ");
    delay(500);
    lcd.clear();
  } else {
    //tich();
    lcd.setCursor(0,1); lcd.print("   Loi thiet bi !  ");
    while (1) { delay(1); }
    
  }
 
  
}


void loop() {
    while (modes == 0){
        //che do hien thi
          attachInterrupt(0,change_mode,FALLING);
          //digitalWrite(BUZZ,0);
          check_id();
          send_data();
          if  (stt_warning == 1){
                digitalWrite(BUZZ,1);
                make_call();
          }
          if  (stt_vantay == 2){
                digitalWrite(BUZZ,1);
                make_call();
          }
          
          else digitalWrite(BUZZ,0);
          
      }
    while (modes == 1)
      {         
               //che do cai dat van tay
                delay(100);
                detachInterrupt(digitalPinToInterrupt(0));
                
                if (millis() - time_mode > 8000) {cmode = 0;modes = 0;}//kiem tra thoi gian
                
                
                lcd.setCursor(0,0); lcd.print("Cai dat van tay?");
                lcd.setCursor(0,1); lcd.print("1.Khong     2.Co");
                
                if (digitalRead(2) == 0)
                {
                    while (digitalRead(2)==0);
                    cmode = 1; // khong
                    modes = 0;
                }
                if (digitalRead(3) == 0)
                {
                    while (digitalRead(3) == 0);
                    cmode = 2; //co
                    times1= millis();
                } 
            while (cmode==1) // khong cai dat
            {
                  detachInterrupt(digitalPinToInterrupt(0));
 
                  tich();
                  lcd.clear();
                  lcd.setCursor(0,0); lcd.print("   Thoat....   ");
                  delay(1000);
                  cmode=0;
                  modes = 0;
                  lcd.clear();
             }
            while(cmode==2)   //co cai dat
            {
                  detachInterrupt(digitalPinToInterrupt(1));
                    // co cai dat{
                  if (millis() - times1 > 8000) { cmode=0;modes = 0;}
                  tich();
                  lcd.clear();
                  lcd.setCursor(0,0); lcd.print("Dang xoa van tay");
                  delay(200);
                  for (int i=0;i<10;i++)
                  {
                    if(finger.deleteModel(i)==FINGERPRINT_OK)
                    lcd.setCursor(6,1); lcd.print(i+1); lcd.print("/10");
                    delay(300); 
                  }
                  lcd.clear(); lcd.print("  Da xoa xong!  ");
                  lcd.setCursor(0,1); lcd.print("1.Thoat  2.Tiep");
                  cmode = 3;
                  times1 = millis();
            }
            while (cmode ==3 ) // tiep tuc
            {
                  
                  if (millis() - times1 > 8000) {cmode=0;modes = 0;}
                  detachInterrupt(digitalPinToInterrupt(0));
                  if (digitalRead(2) == 0)
                  {
                      while(digitalRead(2)==0);
                      lcd.setCursor(0,0); lcd.print("   Thoat....   ");
                      delay(1000);
                      cmode=0;
                      modes = 0;
                      lcd.clear();
                  }
                  if (digitalRead(3) == 0)
                  {
                      while(digitalRead(3)==0);
                      cmode =4; //tiep
                      lcd.clear();
                      times1 = millis();
                  }
                  
              }
            while (cmode == 4)  // chon so luong van tay
            {
                 detachInterrupt(digitalPinToInterrupt(0));
                 lcd.setCursor(0,0);
                 lcd.print("So luong van tay:");
                 lcd.setCursor(0,1);
                 lcd.print(number);
                 lcd.print("       ");
                 lcd.setCursor(12,1);
                 lcd.print("  OK");    
                 if (millis() - times1 > 8000) {cmode=0;modes = 0;}        
                 if (digitalRead(2) == 0)
                 {
                      {
                         while(digitalRead(2)==0);
                         delay(150);
                         number++;
                      }
                 }
                 if (digitalRead(3) == 0)
                 {
                         while(digitalRead(3) ==0);
                         cmode = 5;
                         times1 = millis(); 
                 }
          
              }
            while (cmode == 5) // cai dat van tay
            {
                detachInterrupt(digitalPinToInterrupt(0));
                while( count<=number){
                    
                     if (laymau(count)==-2) 
                      {
                        tich();
                        lcd.clear();
                        lcd.print("Da luu van tay ");
                        lcd.print(count);
                        delay(1500);
                        count++;
                      }
                      else
                      {
                        lcd.clear();
                        lcd.print("*Loi* -> Lam lai");
                        delay(1500);
                        lcd.clear();
                      
                      }
                  }
                  modes = 0;
                  cmode = 0;
                  number = 0;
                  count = 1;
              }
      }  
} 
